# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
from copy import deepcopy
from typing import Any, Dict, List, Optional, Tuple

from mtrl.env.vec_env import VecTaskPython
from mtrl.utils.types import ConfigType

from omegaconf import OmegaConf
from rlgpu.tasks.franka import FrankaCabinet
from isaacgym import gymapi
from isaacgym import gymutil


def parse_sim_params(cfg,physics_engine=gymapi.SIM_PHYSX, device="GPU"):
    # initialize sim
    sim_params = gymapi.SimParams()
    sim_params.dt = 1. / 40.
    sim_params.num_client_threads = 0 #args.slices

    if physics_engine == gymapi.SIM_FLEX:
        if device == "GPU":
            print("WARNING: Using Flex with GPU instead of PHYSX!")
        sim_params.flex.shape_collision_margin = 0.01
        sim_params.flex.num_outer_iterations = 4
        sim_params.flex.num_inner_iterations = 10
    elif physics_engine == gymapi.SIM_PHYSX:
        sim_params.physx.solver_type = 1
        sim_params.physx.num_position_iterations = 4
        sim_params.physx.num_velocity_iterations = 0
        sim_params.physx.num_threads = 4
        sim_params.physx.use_gpu = True
        sim_params.physx.num_subscenes = 0
        sim_params.physx.max_gpu_contact_pairs = 8 * 1024 * 1024

    if device == "GPU":
        sim_params.use_gpu_pipeline = True

    # if sim options are provided in cfg, parse them and update/override above:
    if "sim" in cfg:
        gymutil.parse_sim_config(cfg["sim"], sim_params)

    # Override num_threads if passed on the command line
    # if physics_engine == gymapi.SIM_PHYSX and args.num_threads > 0:
    #     sim_params.physx.num_threads = args.num_threads

    return sim_params

def build_isaac_vec_env(
    config: ConfigType,
    benchmark: None,
    mode: str):

    benchmark_name = "Isaacgym"

    vecenv_config = {}
    sim_device = 'cuda:0'
    ppo_device = 'cuda:0'
    headless = True

    cfg_ig = OmegaConf.create({"env": config.env, "sim": config.sim})
    sim_params = parse_sim_params(cfg_ig)
    task_name = 'FrankaCabinet'

    try:
        task = eval(task_name)(cfg=cfg_ig,
                               sim_params=sim_params,
                               physics_engine=gymapi.SIM_PHYSX,
                               graphics_device=-1 if headless else 0,
                               device=sim_device)
    except NameError as e:
        print('Something went wrong')
        # warn_task_name()
    env = VecTaskPython(task, ppo_device)

    return env, "FrankaCabinet"
